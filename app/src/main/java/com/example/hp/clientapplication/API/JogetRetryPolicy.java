package com.example.hp.clientapplication.API;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;


/**
 * Created by waqar on 14/09/2018.
 */

public class JogetRetryPolicy implements RetryPolicy {

    private JogetResponse.ErrorListener errorListener;

    public JogetRetryPolicy(JogetResponse.ErrorListener errorListener) {
        this.errorListener = errorListener;
    }

    @Override
    public int getCurrentTimeout() {
        return 10000;
    }

    @Override
    public int getCurrentRetryCount() {
        return 5;
    }

    @Override
    public void retry(VolleyError error) throws VolleyError {
        if (errorListener != null) {
            errorListener.onErrorResponse(error);
        }
    }
}
