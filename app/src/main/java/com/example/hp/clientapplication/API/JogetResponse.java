package com.example.hp.clientapplication.API;

/**
 * Created by Shahzaib Shahid on 7/12/2017.
 */

public class JogetResponse<T> {

    public interface Listener<T>{
        void onResponse(T response);
    }

    public interface ErrorListener{
        void onErrorResponse(Exception error);
    }
}
