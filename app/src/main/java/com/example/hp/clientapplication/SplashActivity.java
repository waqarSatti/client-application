package com.example.hp.clientapplication;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;

import com.example.hp.clientapplication.registration.SignupActivity;

public class SplashActivity extends AppCompatActivity {

    /** Duration of wait **/
    private final int SPLASH_DISPLAY_LENGTH = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);



        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {



                /* Create an Intent that will start the Verification-Activity. */
                Intent mainIntent = new Intent(SplashActivity.this, SignupActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
