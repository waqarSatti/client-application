package com.example.hp.clientapplication.model;

import com.example.hp.clientapplication.API.APIManager;
import com.example.hp.clientapplication.API.JogetObjectResponse;
import com.example.hp.clientapplication.API.JogetResponse;
import com.example.hp.clientapplication.ClientApplication;
import com.example.hp.clientapplication.databinding.ActivitySignupBinding;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Random;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.UserRealmProxy;

import static com.example.hp.clientapplication.Utils.Utils.isValidEmail;

public class User extends RealmObject {

    public String email;
    public String uuid;
    public String data;
    public boolean tnc;
    public boolean isLoggedIn=false;
    public boolean isTemp=true;
    public static final String DATA = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static Random RANDOM = new Random();

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        ClientApplication.realm.executeTransaction(realm -> this.uuid=uuid);
        this.uuid = uuid;
    }


    public boolean isTnc() {
        return tnc;
    }

    public void setTnc(boolean tnc) {
        ClientApplication.realm.executeTransaction(realm -> this.tnc=tnc);
        this.tnc = tnc;
    }

    public static String getDATA() {
        return DATA;
    }



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {

        ClientApplication.realm.executeTransaction(realm -> this.email=email);
    }

//    public String getUuid() {
//        String uuid = UUID.randomUUID().toString();
//        return uuid;
//    }
//
//    public void setUuid(String uuid) {
//        ClientApplication.realm.executeTransaction(realm -> this.uuid=uuid);
//    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        ClientApplication.realm.executeTransaction(realm -> this.data=data);
    }

//    public String isTnc(int len) {
//
//        StringBuilder sb = new StringBuilder(len);
//
//        for (int i = 0; i < len; i++) {
//            sb.append(DATA.charAt(RANDOM.nextInt(DATA.length())));
//        }
//
//        return sb.toString();
//    }

//    public void setTnc(boolean tnc) {
//        ClientApplication.realm.executeTransaction(realm -> this.tnc=tnc);
//    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        ClientApplication.realm.executeTransaction(realm -> this.isLoggedIn=loggedIn);
    }

    public boolean isTemp() {
        return isTemp;
    }

    public void setTemp(boolean temp) {
        ClientApplication.realm.executeTransaction(realm -> this.isTemp=temp);
    }


//    public static User getTempInstance() {
//        User user=ClientApplication.realm.where(User.class).equalTo("isTemp", true).findFirst();
//        if (user != null) {
//            return user;
//        }
//
//        ClientApplication.realm.beginTransaction();
//        User tempUser=ClientApplication.realm.createObject(User.class);
//        ClientApplication.realm.commitTransaction();
//        return tempUser;
//    }


//    public static String getRandomId() {
//
//        String uuid = UUID.randomUUID().toString();
//        return uuid;
//    }


//
//    public static String randomString(int len) {
//        StringBuilder sb = new StringBuilder(len);
//
//        for (int i = 0; i < len; i++) {
//            sb.append(DATA.charAt(RANDOM.nextInt(DATA.length())));
//        }
//
//        return sb.toString();
//    }

    public static User loggedInUser(Realm realm) {
        return realm.where(User.class).equalTo("isLoggedIn", true).findFirst();
    }

    public boolean isValid(ActivitySignupBinding binding) {
        boolean isValid=true;


        if (email == null || !isValidEmail(email)) {          // Email verification
            binding.edEmail.setError("Please enter your valid email");
            isValid=false;
        }

        if (uuid == null || uuid.isEmpty()) {
            binding.etuuid.setError("Please enter password");            //Should not be empty or null
            isValid=false;
        }

        if (data == null || data.isEmpty()) {
            binding.etdata.setError("Please enter password");            //Should not be empty or null
            isValid=false;
        }


        return isValid;
    }


    public void submitUser(JogetResponse.Listener<JogetObjectResponse> listener, JogetResponse.ErrorListener errorListener) {
        String url=APIManager.baseURL;

        GsonBuilder builder=new GsonBuilder();
        builder.registerTypeAdapter(UserRealmProxy.class, this.new UserSerializer());
        String payload=builder.create().toJson(this);

        try {
            JSONObject jsonPayload=new JSONObject(payload);
            Type collectionType=new TypeToken<JogetObjectResponse>() {
            }.getType();

            APIManager.loadData(url, jsonPayload, collectionType, listener, errorListener);

        } catch (JSONException e) {
            errorListener.onErrorResponse(e);
        }
    }

    public class UserSerializer implements JsonSerializer<User> {

        @Override
        public JsonElement serialize(User src, Type typeOfSrc, JsonSerializationContext context) {
            final JsonObject jsonObject=new JsonObject();
            jsonObject.addProperty("email", src.getEmail());
            jsonObject.addProperty("uuid",src.getUuid());
            jsonObject.addProperty("data", src.getData());
            jsonObject.addProperty("tnc",src.isTnc());
            return jsonObject;
        }
    }



    //check apiiiiidsf cnwelfon wl
}
