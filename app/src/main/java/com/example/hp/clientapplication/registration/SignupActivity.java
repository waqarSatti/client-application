package com.example.hp.clientapplication.registration;

import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import com.example.hp.clientapplication.R;
import com.example.hp.clientapplication.Utils.Alert;
import com.example.hp.clientapplication.Utils.Utils;
import com.example.hp.clientapplication.databinding.ActivitySignupBinding;
import com.example.hp.clientapplication.model.User;
import com.kaopiz.kprogresshud.KProgressHUD;

public class SignupActivity extends AppCompatActivity {

    ActivitySignupBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        binding.toolbar.setNavigationOnClickListener((v) -> {
            finish();
        });

      //  binding.setUser(User.getTempInstance());
        binding.setHandler(new Handler());

    }


    public class Handler {

        public void prepareToSubmit(View view) {
            if (binding.getUser().isValid(binding) == true ) {
                KProgressHUD hud = Utils.showHud(SignupActivity.this, "Please wait...", "Signup is in progress");
                hud.show();
                binding.getUser().submitUser(response->{
                    hud.dismiss();
                    if (response.isSuccess()){
                        User user = binding.getUser();
                        user.setLoggedIn(true);
                        user.setTemp(false);
                        Alert.showAlert(SignupActivity.this, "SUCCESS", "User account has been created successfully.", "OK", ()->{
                            finish();
                        });
                    }else{
                        Alert.showAlert(SignupActivity.this, "Error", response.message, "OK");
                    }
                }, error -> {
                    hud.dismiss();
                    Alert.showAlert(SignupActivity.this, "Error", Utils.getMessage(error), "OK");
                });
            }
        }
    }
}
