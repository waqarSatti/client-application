package com.example.hp.clientapplication.API;
import com.google.gson.annotations.SerializedName;

/**
 * Created by waqar on 14/08/2017.
 */

public class JogetObjectResponse<T> {
    public String title;

    @SerializedName("msg")
    public String message;

    public String status;
    public String code;

    public T data;

    public boolean isSuccess() {
        return status != null && status.equalsIgnoreCase("SUCCESS") ? true : false;
    }
}
