package com.example.hp.clientapplication.API;
import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.example.hp.clientapplication.ClientApplication;
import com.example.hp.clientapplication.Utils.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.Date;

public class APIManagerget {

    public static final String baseURL = "http://inca.fahmteknologi.com/jogetapi/public/api/view";

    public static void loadProducts(String url, Type collectionType, JogetResponse.Listener listener, JogetResponse.ErrorListener errorListener){
        StringRequest request = new StringRequest(Request.Method.GET, url, (response) -> {

            Log.d("CART_RESPONSE", response);
            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(Date.class, new JogetDateDeserializer());

            Gson gson = builder.create();
            listener.onResponse(gson.fromJson(response, collectionType));
        }, (error) -> {
            String formattedError = Utils.getMessage(error);
            errorListener.onErrorResponse(new Exception(formattedError));
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json;charset=utf-8";
            }
        };
        request.setRetryPolicy( new JogetRetryPolicy(errorListener));
        ClientApplication.getInstance().addToRequestQueue(request);
    }

    public static void loadDataInBackground(String url, Type collectionType, JogetResponse.Listener listener, JogetResponse.ErrorListener errorListener){
        StringRequest request = new StringRequest(Request.Method.GET, url, (response) -> {
            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(Date.class, new JogetDateDeserializer());

            Gson gson = builder.create();
            listener.onResponse(gson.fromJson(response, collectionType));
        }, (error) -> {
            String formattedError = Utils.getMessage(error);
            errorListener.onErrorResponse(new Exception(formattedError));
        }) {


            @Override
            public String getBodyContentType() {
                return "application/json;charset=utf-8";
            }

            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }
        };
        request.setRetryPolicy( new JogetRetryPolicy(errorListener));
        ClientApplication.getInstance().addToRequestQueue(request);
    }
}
