package com.example.hp.clientapplication.API;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;

/**
 * Created by waqar on 21/02/2017.
 */

public class DataBindViewHolder extends RecyclerView.ViewHolder {
    private ViewDataBinding mViewDataBinding;

    public DataBindViewHolder(ViewDataBinding viewDataBinding) {
        super(viewDataBinding.getRoot());

        mViewDataBinding = viewDataBinding;
        mViewDataBinding.executePendingBindings();
    }

    public ViewDataBinding getViewDataBinding() {
        return mViewDataBinding;
    }
}
