package com.example.hp.clientapplication;

import android.app.Application;
import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class ClientApplication  extends Application {

    public static Realm realm;
    public static Context context;
    public static ClientApplication mInstance;
    public RequestQueue requestQueue;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration configuration = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(configuration);
        realm = Realm.getDefaultInstance();

        context = this.getApplicationContext();
        mInstance = this;

        requestQueue = Volley.newRequestQueue(this);

    }

    public static synchronized ClientApplication getInstance() {
        return mInstance;
    }

    public void addToRequestQueue(Request request) {
        requestQueue.add(request);
    }

}
