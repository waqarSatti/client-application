package com.example.hp.clientapplication.Utils;

import android.databinding.BindingAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hp.clientapplication.ClientApplication;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by waqar on 16/11/2017.
 */

public class BindingUtils {
//    @BindingAdapter("image_url")
//    public static void setImageUrl(SimpleDraweeView imageView, String url) {
//        if (url != null) {
//            int density = (int) ClientApplication.context.getResources().getDisplayMetrics().density;
//            ImagePicker.loadImage(url, imageView);
//        }
//    }

//    @BindingAdapter("profile_image")
//    public static void setProfileImage(SimpleDraweeView imageView, String url) {
//        if (url != null) {
//            int density = (int) KemucakApplication.context.getResources().getDisplayMetrics().density;
//            ImagePicker.loadResizedImage(url, imageView, 96*density, 96*density);
//        }
//    }

    @BindingAdapter({"android:src"})
    public static void setImageViewResource(ImageView imageView, int resource) {
        imageView.setImageResource(resource);
    }

    @BindingAdapter("to_date")
    public static void setDate(TextView textView, Date date){
        if(date == null){
            return;
        }

        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy 'at' hh:mm a");
        textView.setText(formatter.format(date));
    }

    @BindingAdapter("to_price")
    public static void setPrice(TextView textView, float price){
        NumberFormat format = NumberFormat.getCurrencyInstance();
        textView.setText("Rp " + format.format(price));
    }

    @BindingAdapter("to_price")
    public static void  setMoney(Button button , float price){
        NumberFormat format = NumberFormat.getCurrencyInstance();
        button.setText("");
    }


    @BindingAdapter("to_newdate")
    public static void setNewDate(TextView textView, Date date){
        if(date == null){
            return;
        }

        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
        textView.setText(formatter.format(date));
    }

}
