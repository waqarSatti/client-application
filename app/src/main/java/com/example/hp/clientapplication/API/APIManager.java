package com.example.hp.clientapplication.API;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.example.hp.clientapplication.ClientApplication;
import com.example.hp.clientapplication.Utils.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.Date;

/**
 * Created by waqar on 14/08/2017.
 */

public class APIManager {

    public static final String baseURL = "https://staging.hellogold.com/api/v3/users/register.json";

    public static void loadData(String url, JSONObject payload, Type collectionType, JogetResponse.Listener listener, JogetResponse.ErrorListener errorListener){
        StringRequest request = new StringRequest(Request.Method.POST, url, (response) -> {

            Log.d("registration_response", response);
            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(Date.class, new JogetDateDeserializer());

            Gson gson = builder.create();
            listener.onResponse(gson.fromJson(response, collectionType));
        }, (error) -> {
            String formattedError = Utils.getMessage(error);
            errorListener.onErrorResponse(new Exception(formattedError));
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    if (payload != null) {
                        return payload.toString().getBytes("utf-8");
                    }
                } catch (UnsupportedEncodingException e) {
                    errorListener.onErrorResponse(e);
                }
                return null;
            }

            @Override
            public String getBodyContentType() {
                return "application/json;charset=utf-8";
            }
        };
        request.setRetryPolicy( new JogetRetryPolicy(errorListener));
        ClientApplication.getInstance().addToRequestQueue(request);
    }

    public static void loadDataInBackground(String url, JSONObject payload, Type collectionType, JogetResponse.Listener listener, JogetResponse.ErrorListener errorListener){
        StringRequest request = new StringRequest(Request.Method.POST, url, (response) -> {
            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(Date.class, new JogetDateDeserializer());

            Gson gson = builder.create();
            listener.onResponse(gson.fromJson(response, collectionType));
        }, (error) -> {
            String formattedError = Utils.getMessage(error);
            errorListener.onErrorResponse(new Exception(formattedError));
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return payload.toString().getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    errorListener.onErrorResponse(e);
                }
                return null;
            }

            @Override
            public String getBodyContentType() {
                return "application/json;charset=utf-8";
            }

            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }
        };
        request.setRetryPolicy( new JogetRetryPolicy(errorListener));
        ClientApplication.getInstance().addToRequestQueue(request);
    }
}
