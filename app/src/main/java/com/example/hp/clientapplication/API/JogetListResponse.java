package com.example.hp.clientapplication.API;

/**
 * Created by waqar on 10/08/2017.
 */

public class JogetListResponse<T> {
    public String title;
    public String message;
    public String status;
    public String code;

    public T[] data;

    public boolean isSuccess() {
        return status.equalsIgnoreCase("SUCCESS") ? true : false;
    }
}
