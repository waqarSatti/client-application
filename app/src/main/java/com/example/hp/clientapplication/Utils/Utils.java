package com.example.hp.clientapplication.Utils;

import android.content.Context;

import com.android.volley.VolleyError;
import com.example.hp.clientapplication.API.VolleyErrorHelper;
import com.example.hp.clientapplication.ClientApplication;
import com.kaopiz.kprogresshud.KProgressHUD;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by waqar on 12/22/2017.
 */

public class Utils {
    public static String getMessage(Exception e) {
        if (e instanceof VolleyError) {
            return VolleyErrorHelper.getMessage(e, ClientApplication.context);
        } else {
            return e.getMessage();
        }
    }

    public static boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

//    public static boolean isValidPhoneNumber(String mobileNumber, String region) {
//
//        if(mobileNumber == null || mobileNumber.isEmpty()){
//            return false;
//        }
//
//        PhoneNumberUtil util = PhoneNumberUtil.getInstance();
//        try {
//            Phonenumber.PhoneNumber phoneNumber = util.parse(mobileNumber, region);
//            PhoneNumberUtil.PhoneNumberType type = util.getNumberType(phoneNumber);
//           if(type == PhoneNumberUtil.PhoneNumberType.MOBILE || type == PhoneNumberUtil.PhoneNumberType.FIXED_LINE){
//               return true;
//           }
//        } catch (NumberParseException e) {
//
//        }
//        return false;
//    }

    public static boolean isValidName( String fullname) {
        String NAME_PATTERN = "^[a-zA-Z0-9_]*$";
        Pattern pattern = Pattern.compile(NAME_PATTERN);
        Matcher matcher = pattern.matcher(fullname);
        return matcher.matches();
    }

    public static KProgressHUD showHud(Context context, String title, String message) {
        return KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(title)
                .setDetailsLabel(message)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

}
